using System.ComponentModel.DataAnnotations;

namespace pgs_2.Models
{
    public class Link
    {
        public int Id {get; set;}
        [Url]
        [Required]
        [StringLength(500, MinimumLength = 3)]
        public string Url {get;set;}
        public string Hash {get;set;}
    }
}