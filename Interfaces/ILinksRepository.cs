using System.Collections.Generic;
using pgs_2.Models;

namespace pgs_2.Interfaces
{
    public interface ILinksRepository
    {
        List<Link> GetLinks();
        string FindLinkForHash(string hash);
        void AddLink(Link book);
        void Delete(Link book);
        void Update(Link book);
    }
}