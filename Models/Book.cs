using System;
using System.ComponentModel.DataAnnotations;

namespace pgs_2.Models
{
    public class Book
    {
        [Required]
        public int Id {get;set;}

        [StringLength(60, MinimumLength = 3)]
        [Required]
        public string Title { get; set; }

        [StringLength(60, MinimumLength = 3)]
        [Required]
        public string Author { get; set; }
    }
}