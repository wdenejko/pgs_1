using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using pgs_2.Models;
using pgs_2.Interfaces;

namespace pgs_2.Repository
{
    public class BooksRepository : IBooksRepository
    {
        private List<Book> _books;

        public BooksRepository()
        {
            _books = new List<Book>
            {
                new Book { Id = 0, Title = "Lód", Author = "Jacek Dukaj" },
                new Book { Id = 1, Title = "Valis", Author = "Philip K. Dick" }
            };
        }

        public void AddBook(Book book) 
        {
            book.Id = _books.Count;
            _books.Add(book);
        }

        public List<Book> GetBooks() 
        {
            return _books;
        }

        public void Delete(Book book) 
        {
            var bookToDelete = _books
                .SingleOrDefault(element => element.Author == book.Author && element.Title == book.Title);
            _books.Remove(bookToDelete);
        }

        public void Update(Book book) 
        {
            var bookToUpdateIndex = _books.FindIndex(element => element.Id == book.Id);
            if(bookToUpdateIndex != -1) 
                _books[bookToUpdateIndex] = book;
        }
    }
}
