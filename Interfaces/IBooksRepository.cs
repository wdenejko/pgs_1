using System.Collections.Generic;
using pgs_2.Models;

namespace pgs_2.Interfaces
{
    public interface IBooksRepository
    {
         List<Book> GetBooks();
         void AddBook(Book book);
         void Delete(Book book);
         void Update(Book book);
    }
}