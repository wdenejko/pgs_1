using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using pgs_2.Models;
using pgs_2.Interfaces;


namespace pgs_2.Controllers
{
    public class RedirectController : Controller
    {
        private ILinksRepository _repository;
        
        public RedirectController(ILinksRepository linkRepository)
        {
            _repository = linkRepository;
        }

        [Route("/{id}")]
        public IActionResult Test(string id)
        {
            return Redirect(_repository.FindLinkForHash(id));
        }
    }
}