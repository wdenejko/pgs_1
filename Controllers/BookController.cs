using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using pgs_2.Models;
using pgs_2.Interfaces;

namespace pgs_2.Controllers
{
    
    public class BookController : Controller
    {
        private IBooksRepository _repository;
        
        public BookController(IBooksRepository booksRepository)
        {
            _repository = booksRepository;
        }

        [HttpGet("Book")]
        public IActionResult Index()
        {
            var books = _repository.GetBooks();
            return View(books);
        }

        
        [HttpPost]
        public IActionResult Create(Book book)
        {
            if (ModelState.IsValid) {
                _repository.AddBook(book);
            }
            return RedirectToAction("Index", "Book");
        }


        [HttpGet]
        public IActionResult Remove(Book book)
        {
            _repository.Delete(book);
            return RedirectToAction("Index", "Book");
        }

        [HttpGet("Book/Edit")]
        public IActionResult Edit(Book book) 
        {
            return View(book);
        }
        
        [HttpPost]
        public IActionResult Update(Book book) 
        {
            _repository.Update(book);
            return RedirectToAction("Index", "Book");
        }
    }
}
