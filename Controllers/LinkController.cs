using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using pgs_2.Models;
using pgs_2.Interfaces;

namespace pgs_2.Controllers
{
    public class LinkController : Controller
    {
        private ILinksRepository _repository;
        
        public LinkController(ILinksRepository linksRepository)
        {
            _repository = linksRepository;
        }

        [HttpGet("Link")]
        public IActionResult Index()
        {
            var books = _repository.GetLinks();
            return View(books);
        }

        [HttpPost]
        public IActionResult Create(Link link)
        {
            if (ModelState.IsValid) {
                _repository.AddLink(link);
                return RedirectToAction("Index", "Link");
            }

            return View(link);
        }

        [HttpGet]
        public IActionResult Delete(Link link)
        {
            _repository.Delete(link);
            return RedirectToAction("Index", "Link");
        }

        [HttpGet]
        public IActionResult Edit(Link link) 
        {
            return View(link);
        }
        
        [HttpPost]
        public IActionResult Update(Link link) 
        {
            _repository.Update(link);
            return RedirectToAction("Index", "Link");
        }
    }
}