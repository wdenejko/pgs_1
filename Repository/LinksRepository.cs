using System.Collections.Generic;
using System.Linq;
using pgs_2.Interfaces;
using pgs_2.Models;
using HashidsNet;

namespace pgs_2.Repository
{
    public class LinksRepository : ILinksRepository
    {
        private List<Link> _links;
        private static Hashids saltIds;

        public LinksRepository()
        {
            saltIds = new Hashids("this is my salt", 8); 
            _links = new List<Link>
            {
                new Link { Id = 0, Url = "http://www.wp.pl", Hash = saltIds.Encode(0)},
                new Link { Id = 1, Url = "http://www.onet.pl", Hash = saltIds.Encode(1) }
            };
        }

        public string FindLinkForHash(string hash) 
        {
            return _links.SingleOrDefault(element => element.Id == saltIds.Decode(hash).FirstOrDefault()).Url;
        }

        
        public void AddLink(Link link)
        {
            link.Id = _links.Count;
            link.Hash = saltIds.Encode(link.Id);
            _links.Add(link);
        }

        public void Delete(Link link)
        {
            var linkToDelete = _links
                .SingleOrDefault(element => element.Url == link.Url && element.Hash == link.Hash);
            _links.Remove(linkToDelete);
        }

        public List<Link> GetLinks()
        {
            return _links;
        }

        public void Update(Link link)
        {
            var linkToUpdateIndex = _links.FindIndex(element => element.Id == link.Id);
            if(linkToUpdateIndex != -1) {
                _links[linkToUpdateIndex].Id = link.Id;
                _links[linkToUpdateIndex].Url = link.Url;
                _links[linkToUpdateIndex].Hash = saltIds.Encode(link.Id);
            }
        }
    }
}